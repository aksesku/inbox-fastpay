<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Default_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    function insert_data($table = '', $datas = '', $return = true) {
//        $this->db->cache_delete();
        $this->db->insert($table, $datas);

//        echo $this->db->last_query();
        if ($return)
            return $this->db->insert_id();
    }

    function update_data($table = '', $datas = '', $wheres = '') {
//        $this->db->cache_delete();
        return $this->db->update($table, $datas, $wheres);
//        echo $this->db->last_query();
    }

    function delete_data($table = '', $wheres = '') {
//        $this->db->cache_delete();
        $this->db->delete($table, $wheres);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function count_all_data($field, $table, $join = "", $where = "", $distinct = "") {
        $out = '';

        $this->db->select($field);
        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }
        $this->db->from($table);
        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($where != "") {
            $this->db->where($where);
        }


//        $rs = $this->db->get();
//        $out = $rs->num_rows();
        $out = $this->db->count_all_results();
//        echo $this->db->last_query();
        return $out;
    }

    function count_all_data_in($field, $table, $join = "", $where = "", $wherein_param = "", $wherein = "", $distinct="") {
        $out = '';

        $this->db->select($field);
        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }
        $this->db->from($table);
        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($where != "") {
            $this->db->where($where);
        }
        if ($wherein != "") {
            $this->db->where_in($wherein_param, $wherein);
        }


//        $rs = $this->db->get();
//        $out = $rs->num_rows();
        $out = $this->db->count_all_results();
        //echo $this->db->last_query();
        return $out;
    }

    

    function list_single_data($field, $table, $join = "", $where = "", $order = "") {
        $out = $this->list_data($field, $table, $join, $where, $order, 1);
//        echo $this->db->last_query();
//	_debug_var($out);
        return _get_raw_item($out, 0);
    }

    function list_data($field, $table, $join, $where = "", $order = "", $limit = "", $offset = "", $distinct = "", $is_case = false) {
        $out = '';

        if ($is_case) {
            $this->db->select($field, false);
        } else {
            $this->db->select($field);
        }
        $this->db->from($table);

        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }

        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($where != "") {
            $this->db->where($where);
        }

        if ($order != "") {
            $this->db->order_by($order);
        }

        if ($limit != '') {
            if ($offset != '')
                $this->db->limit($limit, $offset);
            else
                $this->db->limit($limit);
        }
        $rs = $this->db->get();
//        echo $this->db->last_query();
        if ($rs->num_rows() > 0) {
            $out = $rs->result();
        }

        return $out;
    }

    

    function list_data_in($field, $table, $join, $where, $wherein = "", $order = "", $limit = "", $offset = "", $distinct = "") {
        $out = '';

        $this->db->select($field);
        $this->db->from($table);

        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }

        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($wherein != "") {
            $this->db->where_in($where, $wherein);
        }

        if ($order != "") {
            $this->db->order_by($order);
        }

        if ($limit != '') {
            if ($offset != '')
                $this->db->limit($limit, $offset);
            else
                $this->db->limit($limit);
        }

        $rs = $this->db->get();
//        echo $this->db->last_query();
        if ($rs->num_rows() > 0) {
            $out = $rs->result();
        }

        return $out;
    }

    function get_all_join_data($field, $table, $join, $where = "", $order = "", $distinct = "") {
        $out = '';

        $this->db->select($field);
        $this->db->from($table);

        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }

        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($where != "") {
            $this->db->where($where);
        }

        if ($order != "") {
            $this->db->order_by($order);
        }

        $rs = $this->db->get();

        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row)
                $out[] = $row;
        }

        return $out;
    }

    function get_all_data($table = '', $where = '') {
        $out = array();
        if ($table != '') {
            $this->db->select('*');
            $this->db->from($table);
            if ($where != "") {
                $this->db->where($where);
            }
            $rs = $this->db->get();
            if ($rs->num_rows() > 0) {
//                foreach($rs->result() as $row)
//                    $out[] = $row;
                $out = $rs->result_array();
            }
        }

//        echo $this->db->last_query();
        return $out;
    }

    function get_count_all_data($table = '', $field_key = '', $where = '') {
        $out = 0;
        if ($table != '') {
            $this->db->select($field_key);
            $this->db->from($table);
            if ($where != "") {
                $this->db->where($where);
            }
            $rs = $this->db->get();
            $out = $rs->num_rows();
        }

//        echo $this->db->last_query();
        return $out;
    }

    function get_count_all_join_data($field, $table, $join, $where = "", $distinct = "") {
        $out = '';

        $this->db->select($field);
        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }
        $this->db->from($table);
        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($where != "") {
            $this->db->where($where);
        }

        $rs = $this->db->get();

        $out = $rs->num_rows();

        return $out;
    }

    function get_redirect_last_page() {
        $dir = str_replace('/', '', $this->router->fetch_directory());
        $class = str_replace('/', '', $this->router->fetch_class());
        return site_url(array($dir, $class, 'index', $this->session->userdata('last_paging_offset')));
    }

    

    function list_all_data($table, $limit = "", $offset = "") {
        $out = '';
        $limit = $limit == "" ? $this->config->item("MY_MAX_ROWS_LIMIT") : $limit;
        $offset = $offset == "" ? 0 : $offset;

        $rs = $this->db->get($table, $limit, $offset);

        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row)
                $out[] = $row;
        }
//        echo $this->db->last_query();
        return $out;
    }

    function list_join_data($field, $table, $join, $where = "", $order = "", $limit = "", $offset = "", $distinct = "") {
        $out = '';
        $limit = $limit == "" ? MY_MAX_ROWS_LIMIT : $limit;
        $offset = $offset == "" ? 0 : $offset;

        $this->db->select($field);
        $this->db->from($table);

        if ($distinct != "") {
            $this->db->distinct($distinct);
            $this->db->group_by($distinct);
        }

        if (is_array($join)) {
            foreach ($join as $key => $value) {
                $this->db->join($key, $value, "left");
            }
        }

        if ($where != "") {
            $this->db->where($where);
        }

        if ($order != "") {
            $this->db->order_by($order, 'DESC');
        }


        $this->db->limit($limit, $offset);

        $rs = $this->db->get();
        //echo $this->db->last_query();die();
        $out = array();
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row){
                $out[] = $row;
            }
        }

        return $out;
    }

    function list_data_by_condition($table, $wheres, $orders = '', $limit = '', $offset = '') {
        $out = array();

        $limit = $limit == "" ? $this->config->item("MY_MAX_ROWS_LIMIT") : $limit;

        $offset = $offset == "" ? 0 : $offset;

        $this->db->select('*');
        $this->db->from($table);

        if ($orders != "") {
            $this->db->order_by($orders);
        }

        if ($wheres != "") {
            $this->db->where($wheres);
        }

        $this->db->limit($limit, $offset);

        $rs = $this->db->get_where();

//        echo $this->db->last_query();
        if ($rs->num_rows() > 0) {
            foreach ($rs->result() as $row)
                $out[] = $row;
        }

        return $out;
    }

    

    function get_single_data_by_condition($table = '', $wheres = '') {
        $out = '';
        $rs = $this->db->get_where($table, $wheres, 1, 0);
        if ($rs->num_rows() > 0) {
            $out = $rs->row();
        }

//        echo $this->db->last_query();
        return $out;
    }

    function get_single_data_join_by_condition($field, $table, $join, $where = "") {

        if ($where == "") {
            return "";
        }

        $data = array();
        $rs = $this->list_join_data($field, $table, $join, $where, '', 1, 0);

        if ($rs != "") {
            foreach ($rs as $key => $value) {
                foreach ($value as $keys => $values) {
                    $data[$keys] = $values;
                }
            }
        }
        $data = json_decode(json_encode($data), false);
        return $data;
    }

    function query($query, $bind = array()) {
        return $this->db->query($query, $bind);
    }

    function get_last_query() {
        return $this->db->last_query();
    }

    

    function __destruct() {
        $this->db->close();
    }
    
}
