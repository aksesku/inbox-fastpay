<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class LogAccess{
	private $iduser;
    private $path;
    private $via;
    private $id_log;
    private $response_time;

    public function __construct() {
        //parent::__construct();
        $this->CI =& get_instance();
        $this->CI->load->database();
    }

    public function set_path($path){
    	$this->path = $path;
    	return $this;
    }

    public function set_iduser($iduser){
    	$this->iduser = $iduser;
    	return $this;
    }

    public function set_via($via){
    	$this->via = $via;
    	return $this;
    }

    public function save_log(){
    	$this->CI->db->insert('api_access_log', array(
                        'path' => $this->path,
                        'idapi_access' => $this->iduser,
                        'via' => $this->via
                    ));
    	$this->id_log = $this->CI->db->insert_id();
    }

    public function get_idlog(){
    	return $this->id_log;
    }
}