<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inbox extends SP_Controller {
    
    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Bangkok");
        $this->load->model('default_model');
    }
    
    public function index() {
        die('hello there');
    }
    
    public function create(){
    	$this->load->model('default_model');
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
            $inbox['title'] = strip_tags($data['title']);
	    	$inbox['content'] = htmlentities($data['content']);
	    	$inbox['url_image'] = strip_tags($data['url_image']);
	    	$inbox['url_web'] = strip_tags($data['url_web']);
	    	$inbox['publish_time'] = strip_tags($data['publish_time']);
	    	$inbox['nav_to'] = $data['nav_to'];
	    	$inbox['type'] = strip_tags($data['type']);
            $inbox['slug'] = $this->clean($data['title']);
	    	$push_to = $data['push_to'];

	    	$save_data = $this->default_model->insert_data("mailbox_inbox", $inbox);
	    	if($save_data){
	    		foreach ($push_to as $key => $values) {
                    $add_data = isset($values['add_data']) && is_array($values['add_data']) ? $values['add_data'] : null;
                    unset($values['add_data']);
	    			$values['idmailbox_inbox'] = $save_data;
	    			$save_data_to = $this->default_model->insert_data("mailbox_to", $values);

	    			if($add_data != null){
	    				
                        foreach ($add_data as $key => $value) {
                            $value['mailbox_to_idmailbox_to'] = $save_data_to;
                            $save_data_addt = $this->default_model->insert_data("mailbox_inbox_add", $value);
                        }
	    			}
	    		}
	    		$this->set_result_multiple(array(
	    			"response_code"=>RC_SUCCESS, 
	    			"response_desc"=>RD_SUCCESS,
	    			"data" => array()
	    		));
	    	}else{
	    		$this->set_result_multiple(array(
	    			"response_code"=>RC_INVALID, 
	    			"response_desc"=>RD_INVALID,
	    			"data" => array()
	    		));
	    	}

    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();



    	
    }

    public function count_unread_inbox(){
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
    		$id_outlet = isset($data['user_id']) ? $data['user_id'] : "XX";
    		$filter = isset($data['data']) && is_array($data['data']) ? $data['data'] : array();
    		if($filter['type'] == 'ALL'){
    			unset($filter['type']);
    		}
    		$filter['id_outlet'] = $id_outlet;
    		$filter['is_deleted'] = '0';
    		$filter['is_read'] = '0';

    		$count = $this->default_model->count_all_data("id_outlet", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), $filter);
    		
    		$this->set_result_multiple(array(
	    			"response_code"=>RC_SUCCESS, 
	    			"response_desc"=>RD_SUCCESS,
	    			"data" => array(
	    				"unread_inbox" => $count
	    			)
	    		));
    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();
    }

    public function read_inbox(){
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
    		$id_outlet = isset($data['user_id']) ? $data['user_id'] : "XX";
    		$filter = isset($data['data']) && is_array($data['data']) ? $data['data'] : array();
    		$inboxid = isset($filter['inboxid']) && $filter['inboxid'] !== '' ? $filter['inboxid'] : 0;
    		
    		$count = $this->default_model->count_all_data("id_outlet", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), array("a.idmailbox_inbox" => $inboxid, "id_outlet" => $id_outlet));

    		if($count > 0){
    			$update = $this->default_model->update_data("mailbox_to", array("is_read" => '1', "read_time" => date("Y-m-d H:i:s")), array("idmailbox_inbox" => $inboxid, "id_outlet" => $id_outlet));
    			if($update){
    				$this->set_result_multiple(array(
		    			"response_code"=>RC_SUCCESS, 
		    			"response_desc"=>RD_SUCCESS,
		    			"data" => array()
		    		));
    			}else{
    				$this->set_result_multiple(array(
		    			"response_code"=>RC_INVALID, 
		    			"response_desc"=>RD_INVALID,
		    			"data" => array()
		    		));
    			}
    		}else{
    			$this->set_result_multiple(array(
	    			"response_code"=>RC_INVALID, 
	    			"response_desc"=>"Data inbox tidak ditemukan.",
	    			"data" => array()
	    		));
    		}
    		
    		
    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();
    }

    public function list_inbox(){
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
    		$id_outlet = isset($data['user_id']) ? $data['user_id'] : "XX";
    		$filter = isset($data['data']) && is_array($data['data']) ? $data['data'] : array();
    		if(isset($filter['type']) && $filter['type'] == 'ALL'){
    			unset($filter['type']);
    		}else if(isset($filter['type']) && $filter['type'] == 'NONPROMO'){
                unset($filter['type']);
                $filter['type !='] = 'PROMO';
            }
    		if(isset($filter['offset'])){
    			$offset = $filter['offset'];
    		}else{
    			$offset = 0;
    		}

    		if(isset($filter['limit'])){
    			$limit = $filter['limit'];
    		}else{
    			$limit = "";
    		}
            

			unset($filter['limit']);
			unset($filter['offset']);
    		$filter['id_outlet'] = $id_outlet;
    		$filter['is_deleted'] = '0';
            $filter['publish_time <='] = date("Y:m:d H:i:s");

    		$data_inbox = $this->default_model->list_join_data("b.*, a.is_read", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), $filter, "b.publish_time", $limit, $offset);

            array_walk($data_inbox, function(&$value, $index) {
                $content = str_replace("&nbsp;", "", $value->content);
                $content = html_entity_decode($content);

                $string = trim(preg_replace('/\s+/', ' ', strip_tags($content)));
                $value->content = trim(preg_replace("/&nbsp;/",'',$string));
            });
    		$this->set_result_multiple(array(
	    			"response_code"=>RC_SUCCESS, 
	    			"response_desc"=>RD_SUCCESS,
	    			"data" => $data_inbox
	    		));
    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();
    }


    public function detail_inbox(){
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
    		$id_outlet = isset($data['user_id']) ? $data['user_id'] : "XX";

            if(isset($data['data']) && is_array($data['data'])){
                $add_data = $data['data'];
                if(isset($add_data['inboxid']) && $add_data['inboxid'] !== ''){
                    $param['a.idmailbox_inbox'] = $add_data['inboxid'];
                }
                if(isset($add_data['slug']) && $add_data['slug'] !== ''){
                    $param['slug'] = $add_data['slug'];
                }
                $param['id_outlet'] = $id_outlet;

                $count = $this->default_model->count_all_data("id_outlet", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), $param);

                if($count > 0){
                    $param['a.is_deleted'] = '0';
                    $data_inbox = $this->default_model->get_single_data_join_by_condition("b.*, a.*", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), $param);

                    if($data_inbox){
                        $data_inbox->content = html_entity_decode($data_inbox->content);
                        $data_inbox->customer_phone = '62'.ltrim($data_inbox->customer_phone, "0");

                        $additional_data = $this->default_model->list_join_data("b.*", "mailbox_to a", array("mailbox_inbox_add b" => "a.idmailbox_to=b.mailbox_to_idmailbox_to","mailbox_inbox c" => "a.idmailbox_inbox=c.idmailbox_inbox"), $param);
                        if(!empty($additional_data)){
                            $data_inbox->additional_data = $additional_data;
                        }

                        $this->set_result_multiple(array(
                            "response_code"=>RC_SUCCESS, 
                            "response_desc"=>RD_SUCCESS,
                            "data" => $data_inbox
                        ));
                    }else{
                        $this->set_result_multiple(array(
                            "response_code"=>RC_INVALID, 
                            "response_desc"=>RD_INVALID,
                            "data" => array()
                        ));
                    }
                }else{
                    $this->set_result_multiple(array(
                        "response_code"=>RC_INVALID, 
                        "response_desc"=>"Data inbox tidak ditemukan.",
                        "data" => array()
                    ));
                }
            }else{

            }
    		/*$filter = isset($data['data']) && is_array($data['data']) ? $data['data'] : array();
    		$inboxid = isset($filter['inboxid']) && $filter['inboxid'] !== '' ? $filter['inboxid'] : unset($filter['inboxid']);
            $slug = isset($filter['slug']) && $filter['slug'] !== '' ? $filter['slug'] : unset($filter['slug']);*/
    		
    		

    		
    		
    		
    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();
    }

    public function delete_inbox(){
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
    		$id_outlet = isset($data['user_id']) ? $data['user_id'] : "XX";
    		$filter = isset($data['data']) && is_array($data['data']) ? $data['data'] : array();
    		$inboxid = isset($filter['inboxid']) && $filter['inboxid'] !== '' ? $filter['inboxid'] : 0;
    		
    		$count = $this->default_model->count_all_data("id_outlet", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), array("a.idmailbox_inbox" => $inboxid, "id_outlet" => $id_outlet));

    		if($count > 0){
    			$update = $this->default_model->update_data("mailbox_to", array("is_deleted" => '1'), array("idmailbox_inbox" => $inboxid, "id_outlet" => $id_outlet));
    			if($update){
    				$this->set_result_multiple(array(
		    			"response_code"=>RC_SUCCESS, 
		    			"response_desc"=>RD_SUCCESS,
		    			"data" => array()
		    		));
    			}else{
    				$this->set_result_multiple(array(
		    			"response_code"=>RC_INVALID, 
		    			"response_desc"=>RD_INVALID,
		    			"data" => array()
		    		));
    			}
    		}else{
    			$this->set_result_multiple(array(
	    			"response_code"=>RC_INVALID, 
	    			"response_desc"=>"Data inbox tidak ditemukan.",
	    			"data" => array()
	    		));
    		}
    		
    		
    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();
    }

    public function multi_delete_inbox(){
    	$raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
    	
    	if($data){
    		$id_outlet = isset($data['user_id']) ? $data['user_id'] : "XX";
    		$filter = isset($data['data']) && is_array($data['data']) ? $data['data'] : array();
    		$inboxid = isset($filter['inboxid']) && is_array($filter['inboxid']) ? $filter['inboxid'] : array();
    		$count = $this->default_model->count_all_data_in("id_outlet", "mailbox_to a", array("mailbox_inbox b" => "a.idmailbox_inbox=b.idmailbox_inbox"), array("id_outlet" => $id_outlet), "a.idmailbox_inbox", $inboxid);
    		
    		if($count == count($inboxid)){

    			$this->db->set('is_deleted', '1');
				$this->db->where('id_outlet', $id_outlet);
				$this->db->where_in('idmailbox_inbox', $inboxid);
				$this->db->update('mailbox_to'); 

    			
				$this->set_result_multiple(array(
	    			"response_code"=>RC_SUCCESS, 
	    			"response_desc"=>RD_SUCCESS,
	    			"data" => array()
	    		));
    			
    		}else{
    			$this->set_result_multiple(array(
	    			"response_code"=>RC_INVALID, 
	    			"response_desc"=>"Data inbox tidak ditemukan.",
	    			"data" => array()
	    		));
    		}
    		
    		
    	}else{
    		$this->set_result_multiple(array(
    			"response_code"=>RC_INVALID, 
    			"response_desc"=>RD_INVALID,
    			"data" => array()
    		));
    	}
		$this->print_result();
    }

    public function save_partnerid(){
        $this->load->model('default_model');
        $raw = file_get_contents("php://input");
        $data = @json_decode($raw, true);
        
        if($data){
            $count = $this->default_model->count_all_data("id_outlet", "mailbox_user", "", array("id_outlet" => $data['id_outlet']));
            if($count > 0){
                $save_data = $this->default_model->update_data("mailbox_user", $data, array("id_outlet" => $data['id_outlet']));
                if($save_data){
                    $this->set_result_multiple(array(
                        "response_code"=>RC_SUCCESS, 
                        "response_desc"=>RD_SUCCESS,
                        "data" => array()
                    ));
                }else{
                    $this->set_result_multiple(array(
                        "response_code"=>RC_INVALID, 
                        "response_desc"=>RD_INVALID,
                        "data" => array()
                    ));
                }
            }else{
                $save_data = $this->default_model->insert_data("mailbox_user", $data);
                if($save_data){
                    $this->set_result_multiple(array(
                        "response_code"=>RC_SUCCESS, 
                        "response_desc"=>RD_SUCCESS,
                        "data" => array()
                    ));
                }else{
                    $this->set_result_multiple(array(
                        "response_code"=>RC_INVALID, 
                        "response_desc"=>RD_INVALID,
                        "data" => array()
                    ));
                }
            }

            
        }else{
            $this->set_result_multiple(array(
                "response_code"=>RC_INVALID, 
                "response_desc"=>RD_INVALID,
                "data" => array()
            ));
        }
        $this->print_result();
    }

    function __destruct() {
        $this->db->update("api_access_log", array("response_time" => date('Y-m-d H:i:s')) , array("	idapi_access_log" => $this->logaccess));
        
    }
}
