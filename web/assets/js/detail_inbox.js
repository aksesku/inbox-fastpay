$(document).ready(function(){
	var base_url = window.location.origin;
	var icon = {
    	PULSA : base_url+'/assets/img/ubp-pulsa.png', 
    	GAME : base_url+'/assets/img/ubp-game.png',
    	FINANCE : base_url+'/assets/img/ubp-finance.png',
    	PLN : base_url+'/assets/img/ubp-pln.png',
    	PDAM : base_url+'/assets/img/ubp-pdam.png',
    	TELKOM : base_url+'/assets/img/ubp-telkom.png',
    	TV : base_url+'/assets/img/ubp-tv.png',
    	ZAKAT : base_url+'/assets/img/zakat-logo.png',
    	BPJS : base_url+'/assets/img/bpjs_logo.png'
    };
    var color = {
    	PROMO : {
    		background : 'bg-blue',
    		avatar : base_url+'/assets/img/avatar_promo.png'
    	},
    	SYSTEM : {
    		background : 'bg-blush',
    		avatar : base_url+'/assets/img/avatar_sistem.png'
    	},
    	NOTIFIKASI : {
    		background : 'bg-red',
    		avatar : base_url+'/assets/img/avatar_notif.png'
    	},
    	NEWS : {
    		background : 'bg-amber',
    		avatar : base_url+'/assets/img/avatar_news.png'
    	},
    	TRANSAKSI : {
    		background : 'bg-red',
    		avatar : base_url+'/assets/img/avatar_transaksi.png'
    	},
    	INFO : {
    		background : 'bg-green',
    		avatar : base_url+'/assets/img/avatar_comment.png'
    	}

    };
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	detail_message();
	function detail_message(){
		var slug = $("#slug").val();
		cnf_ajax("/receiver/detail_message", {"slug" : slug}, "GET", {
	        before: function () {
	//            $("#detail_bisnis").html("");
	        },
	        after: function () {
	        },
	        error: function (msg) {
	            console.log(msg);
	        },
	        success: function (data) {
	            var text = data.data.content;
	            var decoded = $('<div/>').html(text).text();
	            $("#content").html(data.data.content);
	            $("#title-message").html(data.data.title);
	            $("#title-message + span.badge").html(data.data.type);
	            $("#title-message + span.badge").addClass((typeof color[data.data.type].background !== 'undefined' ? color[data.data.type].background : 'bg-blue'));
	            $("#delete-message").attr('data-idinbox', data.data.idmailbox_inbox);
	            $("#publish-time").html(format_dates(data.data.publish_time));

	            $("#avatar-inbox").attr('src', (typeof color[data.data.type].avatar !== 'undefined' ? color[data.data.type].avatar : base_url+'/assets/img/avatar_cs.png'));
	            


	            if(data.data.url_web !== '' && data.data.url_web !== null){
	            	$("#content").append("<a href='"+data.data.url_web+"' class='btn btn-danger m-b-20 m-t-20 p-center display-table' target='_blank'>Lihat Selengkapnya</a>")
	            }
	            if(data.data.url_image !== '' && data.data.url_image !== null){
	            	$("#content").before("<img src='"+data.data.url_image+"' class='img content-image'>")
	            }
	            if(data.data.is_read === '0'){
	            	read_message(data.data.idmailbox_inbox);
	            }
	            if(typeof data.data.additional_data !== 'undefined'){

	            	

	            	var customer_datas = '<div class="row customer-data">'+
						                    '<div class="col-md-6">'+
						                        '<label class="add-title">Nama </label> '+data.data.customer_name+
						                        '<label class="add-title">Telepon </label> '+data.data.customer_phone+
						                        '<label class="add-title">Tanggal Tagihan </label> '+format_dates2(data.data.bill_date)+
						                    '</div>'+
						                '</div>';
	                $(".additional-header").before(customer_datas);

	                var title_add = '<h5>Detail Tagihan</h5>';
	            	$(".additional-header").before(title_add);

	                var subtotal = 0, total = 0;
	            	$.each(data.data.additional_data, function(key, value) {
	            		var list = '<div class="row additional-data">'+
				                        '<div class="col-md-1 col-2">'+
				                            '<img src="'+(typeof icon[value.group_product] !== 'undefined' ? icon[value.group_product] : base_url+'/assets/img/ubp-lain.png')+'" class="add-image">'+
				                        '</div>'+
				                        '<div class="col-md-2 col-8">'+value.product_name+'</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">ID Pelanggan </label> '+value.customer_id+
				                        '</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">Nama Pelanggan </label> '+value.customer_name+
				                        '</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">Tagihan </label> Rp. '+number_format(value.nominal)+
				                        '</div>'+
				                    '</div>';
		                    $("#additional-data").append(list);
	                    subtotal = subtotal+parseInt(value.nominal);
	            	})

	            	total = parseInt(subtotal)+parseInt(data.data.admin_fee);
	            	var list_sum = '<div class="row additional-data summary-data">'+
				                        '<div class="col-md-9 align-right d-sm-none d-none d-md-inline-block ">Sub Total</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">Sub Total </label> Rp. '+number_format(subtotal)+
				                        '</div>'+
			                        '</div>'+
			                        '<div class="row additional-data summary-data">'+
				                        '<div class="col-md-9 align-right d-sm-none d-none d-md-inline-block ">Biaya Admin</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">Biaya Admin </label> Rp. '+number_format(data.data.admin_fee)+
				                        '</div>'+
			                        '</div>'+
			                        '<div class="row additional-data summary-data">'+
				                        '<div class="col-md-9 align-right d-sm-none d-none d-md-inline-block ">Cashback</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">Cashback </label> Rp. '+number_format(data.data.cashback)+
				                        '</div>'+
				                    '</div>'+
				                    '<div class="row additional-data summary-data">'+
				                        '<div class="col-md-9 align-right d-sm-none d-none d-md-inline-block ">Total Tagihan</div>'+
				                        '<div class="col-md-3">'+
				                            '<label class="d-md-none d-lg-none d-sm-inline-block add-title">Total Tagihan </label> <b>Rp. '+number_format(total)+'</b>'+
				                        '</div>'+
				                    '</div>'+
			                        '<div class="row additional-data summary-data">'+
				                        '<div class="col-md-9 align-right d-sm-none d-none d-md-inline-block ">Ingatkan</div>'+
				                        '<div class="col-md-3">'+
				                            '<a class="btn btn-primary bg-wa btn-block" target="_blank" href="https://api.whatsapp.com/send?phone='+(data.data.customer_phone.indexOf('0') == 0 ? '62'+data.data.customer_phone.substring(1) : data.data.customer_phone)+'&text='+greeting() + ' Bapak/Ibu '+ data.data.customer_name+'"><i class="zmdi zmdi-whatsapp"></i> &nbsp;Ingatkan</a>'+
				                        '</div>'+
				                    '</div>';
                    $("#additional-data").append(list_sum);
	            }else{
	            	$(".additional-header").remove();
	            }
	        }
	    }, true);
		
		return false;
	}

	function read_message(idinbox){
		if(typeof idinbox !== 'undefined'){
			cnf_ajax("/receiver/read_message", {"idinbox" : idinbox}, "POST", {
		        before: function () {
		//            $("#detail_bisnis").html("");
		        },
		        after: function () {
		        },
		        error: function (msg) {
		        },
		        success: function (data) {
		            //console.log(data);
		        }
		    }, true);
		}
		
		return false;
	}

	$("#delete-message").on("click", function() {
		var that = this;

	    var idinbox = $(that).data("idinbox");

	    Swal.fire({
		  title: 'Hapus Inbox',
		  text: "Apakah Anda yakin akan menghapus data inbox ini?",
		  type: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya',
		  cancelButtonText: 'Tidak'
		}).then((result) => {
		  if (result.value) {
		    	cnf_ajax("/receiver/delete_message", {"idinbox" : idinbox}, "POST", {
			        before: function () {
			        },
			        after: function () {
			        },
			        error: function (msg) {
			            console.log(msg);
			        },
			        success: function (data) {			            
						window.location.replace("/");
			        }
			    }, true);
				
		  }
		})
	    
		return false;
	});
})