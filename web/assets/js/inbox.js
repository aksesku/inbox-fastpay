$(document).ready(function(){
	var base_url = window.location.origin;
	
    var color = {
    	PROMO : {
    		background : 'bg-blue',
    		avatar : base_url+'/assets/img/avatar_promo.png'
    	},
    	SYSTEM : {
    		background : 'bg-blush',
    		avatar : base_url+'/assets/img/avatar_sistem.png'
    	},
    	NOTIFIKASI : {
    		background : 'bg-red',
    		avatar : base_url+'/assets/img/avatar_notif.png'
    	},
    	NEWS : {
    		background : 'bg-amber',
    		avatar : base_url+'/assets/img/avatar_news.png'
    	},
    	TRANSAKSI : {
    		background : 'bg-red',
    		avatar : base_url+'/assets/img/avatar_transaksi.png'
    	},
    	INFO : {
    		background : 'bg-green',
    		avatar : base_url+'/assets/img/avatar_comment.png'
    	}

    };
	
	unread_message();
	load_message("all", 1);
	$("#act_delete_all").hide();
	$('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });

	var processing;
	var page_active = 1;

	var top = $(window).scrollTop();
	var bottom = 0

	$(document).scroll(function(e){
		
		if (processing)
		    return false;

		if ($(window).scrollTop() >= bottom){
		    processing = true; 

			var target = $("a[data-toggle='tab'].active").attr("href");
			
			    $("#"+target.replace("#", "") + " .loading").show('');
	    		load_message(target.replace("#", ""), page_active);
		    
		}
	})
	

	
	function load_message(type, page){
		if(page == 1){
			var limit = 10;
			var offset = 0;
		}else{
			var limit = 10;
			var offset = (parseInt(page)-1)*limit;
		}
		cnf_ajax("/receiver/load_inbox", {"type" : type, "limit" : limit, "offset" : offset}, "GET", {
	        before: function () {
				$("#"+type + " .loading").show('');
	        },
	        after: function () {
	        },
	        error: function (msg) {
	        	$("#"+type + " .loading").hide('');
	        	$("#"+type + " .emptydata").remove();

	        	var count_element = $("#"+type + " li.pages").length;
	        	if(count_element == 0){
	        		var empty = '<div class="row emptydata">'+
	                                '<div class="col-md-12">'+
	                                    '<img src="'+base_url+'/assets/img/notfound.png">'+
	                                    '<h5>Data tidak ditemukan</h5>'+
	                                '</div>'+
	                            '</div>';
	                $("#"+type + " .loading").after(empty);
	        	}

	            
	        },
	        success: function (data) {
                /*console.log(data);*/
                if(page == 1){
                	$("#"+type + " li.pages").remove();
                }
				$("#"+type + " .loading").hide('');
				$("#"+type + " .emptydata").remove();
				$.each(data.data, function(key, value) {
					var text = value.content;
		            var decoded = $('<div/>').html(text).text();
		            

					format_dates(value.publish_time);
					var isi = '<li class="list-group-item pages '+(value.is_read === '0' ? 'unread' : '' )+'">'+
	                                '<div class="media">'+
	                                    '<div class="pull-left">'+
	                                        '<div class="controls">'+
	                                            '<div class="checkbox">'+
	                                                '<input type="checkbox" class="checkdel" value="'+value.idmailbox_inbox+'" data-idinbox="'+value.idmailbox_inbox+'" id="basic_checkbox_'+value.idmailbox_inbox+'">'+
	                                                '<label for="basic_checkbox_'+value.idmailbox_inbox+'"></label>'+
	                                            '</div>'+                                
	                                        '</div>'+
	                                        '<div class="thumb d-sm-none d-none d-md-inline m-r-20"> '+
	                                            '<img src="'+(typeof color[value.type].avatar !== 'undefined' ? color[value.type].avatar : base_url+'/assets/img/avatar_cs.png')+'" class="rounded-circle" alt="">'+
	                                        '</div>'+
	                                    '</div>'+
	                                    '<div class="media-body">'+
	                                        '<div class="media-heading"> <a href="/inbox/'+value.slug+'" class="m-r-10">'+value.title+'</a> <span class="badge '+(typeof color[value.type].background !== 'undefined' ? color[value.type].background : 'bg-blue')+'">'+value.type+'</span> <small class="float-right text-muted"><time class="hidden-sm-down" datetime="2017">'+format_dates(value.publish_time)+'</time></small></div>'+
	                                        '<p class="msg">'+(decoded.match(/\<.+\>/) !== null ? $(decoded).text().trim() : decoded)+'</p>'+   
	                                        '<div class="float-right action-hover">';
	                                        if(value.is_read === '0'){
	                                        	isi += '<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Tandai Sudah Dibaca" data-idinbox="'+value.idmailbox_inbox+'" class="markread_inbox"><i class="zmdi zmdi-email-open"></i> </a>';
	                                        }
                                            isi +='<a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Hapus" data-idinbox="'+value.idmailbox_inbox+'"  class="delete_inbox"><i class="zmdi zmdi-delete"></i></a>'+
                                            '</div>'+
	                                    '</div>'+
	                                '</div>'+
	                            '</li>';
                    $("#"+type + " .loading").before(isi);
				});
				$('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
				page_active = page_active+1;
				processing = false;
				bottom = $(document).height() - ($(window).height()*0.75);
	        }
	    }, true);
		
		return false;
	}

	$("#list_inbox").on("click", ".delete_inbox", function() {
		var that = this;

	    var idinbox = $(that).data("idinbox");

	    Swal.fire({
		  title: 'Hapus Inbox',
		  text: "Apakah Anda yakin akan menghapus data inbox ini?",
		  type: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya',
		  cancelButtonText: 'Tidak'
		}).then((result) => {
		  if (result.value) {
		    	cnf_ajax("/receiver/delete_message", {"idinbox" : idinbox}, "POST", {
			        before: function () {
			        },
			        after: function () {
			        },
			        error: function (msg) {
			            console.log(msg);
			        },
			        success: function (data) {			            
						$(that).parents(".list-group-item").fadeOut();
			        }
			    }, true);
				
		  }
		})
	    
		return false;
	});
	$("#list_inbox").on("click", ".markread_inbox", function() {
		var that = this;
	    var idinbox = $(this).data("idinbox");

	    cnf_ajax("/receiver/read_message", {"idinbox" : idinbox}, "POST", {
	        before: function () {
	//            $("#detail_bisnis").html("");
	        },
	        after: function () {
	        },
	        error: function (msg) {
	            console.log(msg);
	        },
	        success: function (data) {
	            console.log(data);
				$(that).parents(".list-group-item").removeClass("unread");
				$(that).hide();
				unread_message();
	        }
	    }, true);
		
		return false;
	});

	$("#list_inbox").on("input", ".checkdel", function() {
		var that = this;
	    var idinbox = $(this).data("idinbox");
	    var count_element = $(".checkdel").length;
	    var checked_element = $(".checkdel:checked").length;

	    if(count_element == checked_element){
	    	$("#deleteall"). prop("checked", true);
	    }else{
	    	$("#deleteall"). prop("checked", false);
	    }

	    if(checked_element > 0){
	    	$("#act_delete_all").show();
	    }else{
	    	$("#act_delete_all").hide();
	    }
	    
		return false;
	});

	$("#act_delete_all").on("click", function() {
		

		var checked_element = $(".checkdel:checked");
		var id_checked = [];
		$.each(checked_element, function(key, value) {
			
			var idinbox = $(value).data("idinbox");
			id_checked.push(idinbox);
		})

		Swal.fire({
		  title: 'Hapus Inbox',
		  text: "Apakah Anda yakin akan menghapus data inbox?",
		  type: 'question',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya',
		  cancelButtonText: 'Tidak'
		}).then((result) => {
		  if (result.value) {
		    	cnf_ajax("/receiver/multidelete_message", {"idinbox" : id_checked}, "POST", {
			        before: function () {
			//            $("#detail_bisnis").html("");
			        },
			        after: function () {
			        },
			        error: function (msg) {
			            console.log(msg);
			        },
			        success: function (data) {
			            $(".checkdel:checked").parents(".list-group-item").remove();	
			            var type = $("a[data-toggle='tab'].active").attr("href");
						var count_element = $("#"+type.replace("#", "") + " li.pages").length;
				    	if(count_element == 0){
				    		var empty = '<div class="row emptydata">'+
				                            '<div class="col-md-12">'+
				                                '<img src="'+base_url+'/assets/img/notfound.png">'+
				                                '<h5>Data tidak ditemukan</h5>'+
				                            '</div>'+
				                        '</div>';
				            
				            $("#"+type.replace("#", "") + " .loading").after(empty);
				    	}	
			        }
			    }, true);
		  }
		})
		
		return false;
	});

	$("#deleteall").on("input", function() {
		

	    if($(this).is(":checked")){
	    	$(".checkdel"). prop("checked", true);
	    }else{
	    	$(".checkdel"). prop("checked", false);
	    }

	    var checked_element = $(".checkdel:checked").length;
	    if(checked_element > 0){
	    	$("#act_delete_all").show();
	    }else{
	    	$("#act_delete_all").hide();
	    }
		
		return false;
	});

	$("#act_reload").on("click", function() {
		
		var target = $("a[data-toggle='tab'].active").attr("href");
	    load_message(target.replace("#", ""), 1);
	});

	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		page_active = 1;
	  var target = $(e.target).attr("href"); // activated tab
	  load_message(target.replace("#", ""), 1);
	});


	function unread_message(){

		cnf_ajax("/receiver/unread_message", {}, "GET", {
	        before: function () {
	//            $("#detail_bisnis").html("");
	        },
	        after: function () {
	        },
	        error: function (msg) {
	            console.log(msg);
	        },
	        success: function (data) {
	            
	            if(data.data.unread_inbox > 0){
					$("#unread-all").html(data.data.unread_inbox);

	            }else{
	            	$("#unread-all").hide();
	            }
	        }
	    }, true);
		
		return false;
	}
})