$(document).ready(function(){
	$("#login").on("click", function(){
		var that = this;
		var id_outlet = $("#id_outlet").val();
		var password = $("#password").val();

		cnf_ajax("/receiver/login", {"id_outlet" : id_outlet, "password" : password}, "POST", {
	        before: function () {
				$(that).attr("disabled", true);
				$(that).text("Proses....");
	        },
	        after: function () {
	        	$(that).attr("disabled", false);
				$(that).text("Masuk");
	        },
	        error: function (msg) {
	            console.log(msg);
	        },
	        success: function (data) {
				location.reload(true);
	        }
	    }, true);
		
		return false;
	})
})