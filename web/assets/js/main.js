function number_format(money) {
    var f = parseFloat(money);
    if (isNaN(f)) {
        f = 0;
    }
    return f.toFixed(0).replace(/./g, function(c, i, a) {
        return (i && c !== "." && !((a.length - i) % 3)) ? '.' + c : (c === "." ? ',' + c.substring(1) : c);
    });
}

function convert_date_entity(type, value) {
    if (type == "month") {
        switch (value) {
            case 0:
                return "Januari";
            case 1:
                return "Februari";
            case 2:
                return "Maret";
            case 3:
                return "April";
            case 4:
                return "Mei";
            case 5:
                return "Juni";
            case 6:
                return "Juli";
            case 7:
                return "Agustus";
            case 8:
                return "September";
            case 9:
                return "Oktober";
            case 10:
                return "November";
            case 11:
                return "Desember";
        }
    } else if (type === "day") {
        switch (value) {
            case 0:
                return "Minggu";
            case 1:
                return "Senin";
            case 2:
                return "Selasa";
            case 3:
                return "Rabu";
            case 4:
                return "Kamis";
            case 5:
                return "Jum'at";
            case 6:
                return "Sabtu";
        }
    }
}

function format_dates(userDate) {
    var today = new Date(),
    yr = today.getFullYear(),
            month = today.getMonth() < 10 ? '0' + today.getMonth() : today.getMonth(),
            day = today.getDate() < 10 ? '0' + today.getDate() : today.getDate(),
            hour = today.getHours(),
            minutes = today.getMinutes(),
            newDate = day + ' ' + convert_date_entity('month', today.getMonth()) + ' ' + yr;

    var yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);
    var yr = yesterday.getFullYear(),
            month = yesterday.getMonth() < 10 ? '0' + yesterday.getMonth() : yesterday.getMonth(),
            day = yesterday.getDate() < 10 ? '0' + yesterday.getDate() : yesterday.getDate(),
            hour = yesterday.getHours(),
            minutes = yesterday.getMinutes(),
            newDate3 = day + ' ' + convert_date_entity('month', yesterday.getMonth()) + ' ' + yr;

    var date = new Date(userDate),
            yr = date.getFullYear(),
            month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
            hour = date.getHours(),
            minutes = date.getMinutes(),
            newDate2 = day + ' ' + convert_date_entity('month', date.getMonth()) + ' ' + yr;

    $today = new Date();
    $yesterday = new Date($today);
    $yesterday.setDate($today.getDate() - 1);

    if(newDate === newDate2){
        newDates = "Hari Ini, "+ format_two_digits(hour) +":"+ format_two_digits(minutes);
    }else if(newDate3 === newDate2){
        newDates = "Kemarin, "+ format_two_digits(hour) +":"+ format_two_digits(minutes);
    }else{
        newDates = newDate2 +", "+ format_two_digits(hour) +":"+ format_two_digits(minutes);
    }

    return newDates;
}

function format_dates2(userDate) {
    var date = new Date(userDate),
            yr = date.getFullYear(),
            month = date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth(),
            day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate(),
            hour = date.getHours(),
            minutes = date.getMinutes(),
            newDate2 = convert_date_entity('day', date.getDay()) + ', ' + day + ' ' + convert_date_entity('month', date.getMonth()) + ' ' + yr;

    return newDate2;
}

function greeting(){
    var today = new Date(),
    hour = today.getHours();
    var greet = "Selamat Pagi";
    if(hour < 10){
        greet = "Selamat Pagi";
    }else if(hour >= 10 && hour < 15){
        greet = "Selamat Siang";
    }else if(hour >= 10 && hour < 18){
        greet = "Selamat Sore";
    }else{
        greet = "Selamat Malam";
    }
    return greet;
}

function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
}

function parseURL(url) {
    var parser = document.createElement('a'),
            searchObject = {},
            queries, split, i;
    // Let the browser do the work
    parser.href = url;
    // Convert query string to object
    queries = parser.search.replace(/^\?/, '').split('&');
    for (i = 0; i < queries.length; i++) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
    };
}

function htmlEntityEncode(txt) {
    return $('<div />').text(txt).html();
}

function htmlEntityDecode(txt) {
    return $('<div />').html(txt).text();
}
function cnf_ajax(url, data, method, callback, hide_progress) {
    var loader = null;
    return $.ajax(url, {
        data: data,
        method: method,
        dataType: 'json',
        statusCode: {
            404: function() {
                $.growl.error({ message: "Tidak dapat tersambung ke server" });
            }
        },
        beforeSend: function(xhr) {
            
            if (typeof callback["before"] === "function") {
                callback["before"]();
            }
        }
    }).done(function(data) {
        
        if (!data.success && typeof callback["error"] === "function") {
            callback["error"](data.result);
        } else if (typeof callback["success"] === "function") {
            callback["success"](data);
        }
    }).fail(function() {
        if (typeof callback["error"] === "function") {
            callback["error"]("Tidak dapat tersambung ke server");
        }
    }).always(function() {
        
        if (typeof callback["after"] === "function") {
            callback["after"]();
        }
    });
}