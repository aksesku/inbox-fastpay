<script src="<?=base_url("assets/js/detail_inbox.js")?>" type="text/javascript"></script>
<div id="snippetContent">
<section class="content inbox">
    <div class="container-fluid">
        <div class="row clearfix">

            <div class="col-lg-12">
                <div class="card action_bar">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-13">
                                <div class="header-action">
                                    <a href="/" data-toggle="tooltip" data-placement="top" title="Kembali"><i class="zmdi zmdi-arrow-left"></i> </a>
                                </div>
                                <div class="thumb d-sm-none d-none d-md-inline m-r-20 avatar"> 
                                    <img src="" alt="" id="avatar-inbox">
                                    <input type="hidden" id="slug" value="<?=$slug?>">
                                </div>
                                <label>
                                    <b id="title-message"></b> 
                                    <span class="badge "></span>
                                    <small id="publish-time"></small>
                                </label>
                                <div class="header-action action-border float-right">
                                    <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Hapus" id="delete-message"><i class="zmdi zmdi-delete"></i> </a>
                                </div>
                            <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-md-12">
                <p id="content" class="overflow-auto"></p>
                
                <div id="additional-data">
                    <div class="row d-sm-none d-none d-md-flex additional-header">
                        
                        <div class="col-md-3 col-8">
                            Produk
                        </div>
                        <div class="col-md-3">
                            ID Pelanggan
                        </div>
                        <div class="col-md-3">
                            Nama Pelanggan
                        </div>
                        <div class="col-md-3">
                            Tagihan
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-6">

            </div>
        </div>
    </div>
</section>
</div>