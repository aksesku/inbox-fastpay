<?php
$this->load->helper('url');
$this->load->model('message_inbox');
$data = $this->message_inbox->view_all();

//$query = $this->db->query("select * from message_inbox");

$kolom = array('sender', 'receiver', 'content', 'id_modul', 'via', 'is_sent', 'date_created', 'sent_date');
//$kolom = array('id', 'mid', 'step', 'sender', 'receiver', 'content', 'id_modul', 'via', 'is_sent', 'date_created', 'sent_date');
?>
<html>
    <head>    
        <!-- DataTables CSS -->
        <link rel="stylesheet" type="text/css" href=<?php echo base_url() . 'assets/DataTables/media/css/jquery.dataTables.css' ?>>
        <link rel="stylesheet" type="text/css" href=<?php echo base_url() . 'assets/DataTables/examples/resources/syntax/shCore.css' ?>>
        <link rel="stylesheet" type="text/css" href=<?php echo base_url() . 'assets/DataTables/examples/resources/syntax/demo.css' ?>>

        <!-- jQuery -->
        <script type="text/javascript" language="javascript" src=<?php echo base_url() . 'assets/DataTables/media/js/jquery.js' ?> ></script>

        <!-- DataTables -->
        <script type="text/javascript" language="javascript" src=<?php echo base_url() . 'assets/DataTables/media/js/jquery.dataTables.js' ?> ></script>

        <script>
            $(document).ready(function() {
                $('#table_id').DataTable({
                        'aoColumnDefs': [
                                {
                                    'bSortable': false,
                                    'aTargets': -1
                                }
                        ]
                    });
            });
        </script>
    </head>

    <body>

        <a href="./insert"><h4>Tambah Data</h4></a>

        <table id="table_id" style="width: 90%">
            <thead>
                <?php
                for ($i = 0; $i < count($kolom); $i++) {
                    echo '<td>' . $kolom[$i] . '</td>';
                }
                ?>
            <td>Actions</td>
        </thead>
        <tbody>
            <?php
            foreach ($data->result() as $row) {
                $id = array('id' => $row->id_message);
                echo
                '<tr>'
//                    . '<td>' . $row->id_message . '</td>'
//                    . '<td>' . $row->mid . '</td>'
//                    . '<td>' . $row->step . '</td>'
                . '<td width=10%>' . $row->sender . '</td>'
                . '<td width=10%>' . $row->receiver . '</td>'
                . '<td width=20%>' . substr($row->content, 0, 50) . '...</td>'
                . '<td width=10%>' . $row->id_modul . '</td>'
                . '<td width=10%>' . $row->via . '</td>'
                . '<td width=5% align=center>' . $row->is_sent . '</td>'
                . '<td width=10%>' . $row->date_created . '</td>'
                . '<td width=10%>' . $row->sent_date . '</td>'
                . '<td width=15%><a href=./edit/'.$row->id_message.'>edit</a> | <a href=./delete/'.$row->id_message.'>del</a></td>'
                . '</tr>';
            }
            ?>
        </tbody>
    </table>
</body>
</html>