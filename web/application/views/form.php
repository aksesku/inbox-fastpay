<?php
$this->load->helper('form');

if (isset($id)) {
    $this->load->model('message_inbox');
    $data = $this->message_inbox->get($id)->row();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome to CodeIgniter</title>

        <style type="text/css">

            ::selection{ background-color: #E13300; color: white; }
            ::moz-selection{ background-color: #E13300; color: white; }
            ::webkit-selection{ background-color: #E13300; color: white; }

            body {
                background-color: #fff;
                margin: 40px;
                font: 13px/20px normal Helvetica, Arial, sans-serif;
                color: #4F5155;
            }

            a {
                color: #003399;
                background-color: transparent;
                font-weight: normal;
            }

            h1 {
                color: #444;
                background-color: transparent;
                border-bottom: 1px solid #D0D0D0;
                font-size: 19px;
                font-weight: normal;
                margin: 0 0 14px 0;
                padding: 14px 15px 10px 15px;
            }

            code {
                font-family: Consolas, Monaco, Courier New, Courier, monospace;
                font-size: 12px;
                background-color: #f9f9f9;
                border: 1px solid #D0D0D0;
                color: #002166;
                display: block;
                margin: 14px 0 14px 0;
                padding: 12px 10px 12px 10px;
            }

            #body{
                margin: 0 15px 0 15px;
            }

            p.footer{
                text-align: right;
                font-size: 11px;
                border-top: 1px solid #D0D0D0;
                line-height: 32px;
                padding: 0 10px 0 10px;
                margin: 20px 0 0 0;
            }

            #container{
                margin: 10px;
                border: 1px solid #D0D0D0;
                -webkit-box-shadow: 0 0 8px #D0D0D0;
            }
        </style>
    </head>
    <body>

        <div id="container">
            <h1>Send Message</h1>

            <div id="body">
                <?php
                $attributes = array('name' => 'fmessage');
                echo (isset($id)) ? form_open('welcome/update', $attributes) : form_open('welcome/send', $attributes);

                $in_step = array('name' => 'step', 'maxlength' => '100', 'size' => '50', 'value' => '1');
                if (isset($id))
                    $in_step['value'] = $data->step;
                echo '<p>Step</p>';
                echo form_input($in_step);

                $in_sender = array('name' => 'sender', 'maxlength' => '100', 'size' => '50');
                if (isset($id))
                    $in_sender['value'] = $data->sender;
                echo '<p>Sender</p>';
                echo form_input($in_sender);

                $in_receiver = array('name' => 'receiver', 'maxlength' => '100', 'size' => '50');
                if (isset($id))
                    $in_receiver['value'] = $data->receiver;
                echo '<p>Receiver</p>';
                echo form_input($in_receiver);

                $in_content = array('name' => 'content', 'rows' => '3', 'cols' => '50');
                if (isset($id))
                    $in_content['value'] = $data->content;
                echo '<p>Content</p>';
                echo form_textarea($in_content);

                $in_id_modul = array('name' => 'id_modul', 'maxlength' => '100', 'size' => '50');
                if (isset($id))
                    $in_id_modul['value'] = $data->id_modul;
                echo '<p>Modul</p>';
                echo form_input($in_id_modul);

                $in_via = array('name' => 'via', 'maxlength' => '100', 'size' => '50');
                if (isset($id))
                    $in_via['value'] = $data->via;
                echo '<p>Via</p>';
                echo form_input($in_via);

                echo (isset($id)) ? '' : form_hidden('date_created', date('c', time()));
                echo (isset($id)) ? form_hidden('id_message', $data->id_message) : '';

                echo '<p>' . form_submit('send', 'Send') . '</p>';
                ?>
            </div>

            <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
        </div>

    </body>
</html>
