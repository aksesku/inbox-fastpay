<script src="<?=base_url("assets/js/inbox.js")?>" type="text/javascript"></script>
<div id="snippetContent">
        
        <section class="content inbox">
            <div class="container-fluid">
                <div class="row clearfix">

                    <div class="col-lg-12">
                        <div class="card action_bar">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="checkbox inlineblock delete_all" data-toggle="tooltip" data-placement="top" title="Tandai Semua">
                                            <input id="deleteall" type="checkbox">
                                            <label for="deleteall">  </label>
                                        </div>
                                        <div class="header-action">
                                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Hapus" id="act_delete_all" class="m-t-5 fs-20"><i class="zmdi zmdi-delete"></i> </a>

                                            <a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Refresh" id="act_reload" class="m-t-5 fs-20"><i class="zmdi zmdi-refresh"></i> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab" aria-controls="all" aria-selected="true"><i class="zmdi zmdi-inbox"></i> &nbsp; Semua <label class="badge bg-red" id="unread-all"></label></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="promo-tab" data-toggle="tab" href="#promo" role="tab" aria-controls="promo" aria-selected="false"><i class="zmdi zmdi-tag"></i> &nbsp; Promo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="nonpromo-tab" data-toggle="tab" href="#nonpromo" role="tab" aria-controls="nonpromo" aria-selected="false"><i class="zmdi zmdi-info-outline"></i> &nbsp; Informasi</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                                <ul class="mail_list list-group list-unstyled" id="list_inbox">
                                    <li class="list-group-item loading">
                                        <div class="row">
                                            <div class="col-md-1 col-1">
                                                <div class="placeholder1 animated-background"></div>
                                            </div>
                                            <div class="col-md-9 col-9">
                                                <div class="placeholder2 animated-background"></div>
                                                <div class="placeholder3 animated-background"></div>
                                            </div>
                                        </div>
                                        
                                    </li>
                                        
                                        
                                    
                                </ul>
                                
                            </div>
                            <div class="tab-pane fade" id="promo" role="tabpanel" aria-labelledby="promo-tab">
                                <ul class="mail_list list-group list-unstyled">
                                    <li class="list-group-item loading">
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="placeholder1 animated-background"></div>
                                            </div>
                                            <div class="col-9">
                                                <div class="placeholder2 animated-background"></div>
                                                <div class="placeholder3 animated-background"></div>
                                            </div>
                                        </div>
                                        
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="nonpromo" role="tabpanel" aria-labelledby="nonpromo-tab">
                                <ul class="mail_list list-group list-unstyled">
                                    <li class="list-group-item loading">
                                        <div class="row">
                                            <div class="col-1">
                                                <div class="placeholder1 animated-background"></div>
                                            </div>
                                            <div class="col-9">
                                                <div class="placeholder2 animated-background"></div>
                                                <div class="placeholder3 animated-background"></div>
                                            </div>
                                        </div>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-lg-6">

                    </div>
                </div>
            </div>
        </section>
    </div>