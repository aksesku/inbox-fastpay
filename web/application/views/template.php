<html lang="en">

<head itemscope="" itemtype="http://schema.org/WebSite">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sentra Bisnis FASTPAY – Bisnis Terbaik, Mudah dan Pasti Untung!</title>      
    <meta name="description" content="Wujudkan impian Anda dengan Menjadi Mitra Toko Modern Fastpay. Bisnis terlengkap & pasti untung 8 layanan dalam 1 bisnis">        
    <meta name="keywords" content="bisnis ppob,peluang usaha,bisnis ppob terbaik,pembayaran online,agen ppob,loket pembayaran">
    <meta name="google-site-verification" content="iUeSm8ymBWV9bE5KuqR4--W5Pm3pk_HiJOyaMO7z17A"/>
    <meta name="msvalidate.01" content="CB37E1872BED5C869A88AE2AFB6ACA65"/>
    <link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.fastpay.co.id"/>
    <link rel="publisher" href="https://plus.google.com/+SentraBisnisFASTPAYOfficial"/>
    <meta name="author" content="https://www.facebook.com/SBFPusat/">
    <meta name='robots' content='index, follow, noodp, noydir'/>
    <meta name='geo.country' content='id'/>
    <meta name='revisit-after' content='1 days'/>
    <meta name='geo.placename' content='Indonesia'/>
    <meta content='id_id' property='og:locale'/>
    <meta property="fb:app_id" content="142251606458026"/>
    <meta property="og:type" name="ogWebsite" content="website"/>

    <meta property="og:title" name="ogTitle" content="Daftar FASTPAY Bonus Saldo Hingga Rp 400.000?, Fitur Terlengkap, Keuntungan Besar tiap Transaksi,"/>
    <meta property="og:image" name="ogImage" content="https://www.fastpay.co.id/blog/wp-content/uploads/2019/04/daftar-bonus-hingga-400k.jpeg"/>
    <meta property="og:site_name" name="ogSiteName" content="Sentra Bisnis Fastpay"/>
    <meta property="og:domain" name="ogDomain" content="https://www.fastpay.co.id/"/>       
    <meta property="og:description" name="ogDescription" content="Wujudkan impian Anda dengan Menjadi Mitra Toko Modern Fastpay. Bisnis terlengkap & pasti untung 8 layanan dalam 1 bisnis"/>
    <meta property="og:url" name="ogUrl" content="https://www.fastpay.co.id/"/>
     
    <link href="https://www.fastpay.co.id/assets/images/ico/icon.jpg" rel="shortcut icon" type="image/x-icon">

    <script src="<?=base_url("assets/js/jquery.js")?>" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <script src="<?=base_url("assets/js/jquery.growl.js")?>" type="text/javascript"></script>
    <script src="<?=base_url("assets/js/sweetalert2.all.min.js")?>" type="text/javascript"></script>
    <script src="<?=base_url("assets/js/main.js")?>" type="text/javascript"></script>
    

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="<?=base_url("/assets/css/inbox.css")?>">
    <link rel="stylesheet" href="<?=base_url("/assets/css/dashboard.css")?>">
    <link rel="stylesheet" href="<?=base_url("/assets/css/jquery.growl.css")?>">
    <link rel="stylesheet" href="<?=base_url("/assets/css/sweetalert2.min.css")?>">
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-blue flex-md-nowrap p-0 shadow">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
          <img src="https://www.fastpay.co.id/assets/images/logo-fastpay.png">
      </a>
      
      <ul class="navbar-nav px-3 header-menu-right">
        <li class="nav-item text-nowrap">
          <a href="/logout" data-toggle="tooltip" data-placement="top" title="Keluar" id="" class="fs-20"><i class="zmdi zmdi-power"></i> </a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                      <li class="nav-item">
                        <a class="nav-link active" href="#">
                          <i class="zmdi zmdi-inbox"></i> &nbsp;
                          Inbox
                        </a>
                      </li>
                    </ul>
                </div>
            </nav>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4 m-t-30">
                {CONTENT}

                <!-- Footer -->
                <footer class="page-footer font-small blue" style="border-top: solid 1px #555;margin-top: 50px;">

                  <!-- Copyright -->
                  <div class="footer-copyright text-center py-3" style="font-size: 14px;margin: 20px;">© 2020 Copyright:
                    <a href="https://fastpay.co.id"> Fastpay</a>
                  </div>
                  <!-- Copyright -->

                </footer>
                <!-- Footer -->
            </main>
        </div>

    </div>
    
    
</body>
</html>