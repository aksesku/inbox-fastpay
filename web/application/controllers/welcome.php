<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('message_inbox');
        $this->load->helper('url');
    }

    public function index() {
        $this->load->view('message_inbox');
    }

    public function view() {
        $this->load->view('message_inbox');
    }
    
    public function insert() {
        $this->load->view('form');
    }

    public function edit($id) {
        $param = array('id' => $id);
        $this->load->view('form', $param);
    }
    
    public function update() {
        if (filter_input(INPUT_POST, 'send')) {
            $data = filter_input_array(INPUT_POST);
            unset($data['send']);
            $this->array_insert($data, array_search('via', array_keys($data)) + 1, array('is_sent' => 2));

//            print_r($data);
            $param = array('id_message' => $data['id_message']);
            unset($data['id_message']);
            
            $this->message_inbox->update($data, $param);
            header('Location: ' . base_url() . 'index.php/welcome/view');
        }
    }

    public function delete($id) {
        $param = array('id_message' => $id);
        $this->message_inbox->delete($param);
        header('Location: ' . base_url() . 'index.php/welcome/view');
    }

    public function send() {
        if (filter_input(INPUT_POST, 'send')) {
            $data = filter_input_array(INPUT_POST);
            unset($data['send']);
            $data['sent_date'] = date('c', time());
            $this->array_insert($data, array_search('via', array_keys($data)) + 1, array('is_sent' => 2));
            $this->array_insert($data, 0, array('mid' => $this->getMid()));

//            print_r($data);
            $this->message_inbox->insert($data);
            header('Location: ' . base_url() . 'index.php/welcome/view');
        }
    }

    function getMid() {
        $SQL = " select nextval('message_inbox_mid_seq') as mid";
        $resultSet = $this->db->query($SQL);
        $result = $resultSet->row();
        $mid = $result->mid;
        return $mid;
    }

    function array_insert(&$array, $position, $insert_array) {
        $first_array = array_splice($array, 0, $position);
        $array = array_merge($first_array, $insert_array, $array);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */