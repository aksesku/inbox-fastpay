<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends SP_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');

    }
    
    public function index() {
    	
			$this->view('inbox');
        
    }

    public function login(){
    	$logged_in = $this->session->userdata('id_outlet');
        
		if ($logged_in)
		{
	        header("Location: /");
		}else{
    		$this->load->view('login');
		}
    }

    public function inbox($slug){
        //$slug = $this->input->get("slug");
        $data = array(
            "slug" => $slug
        );
        $this->view('detail_inbox', $data);
    }
    
    public function logout(){
        $this->session->sess_destroy();
        header("Location: login");
    }
}
