<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Receiver extends SP_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');

        $this->load->library('CNFMessage');
        $this->load->library('CNFApi');
    }

    public function login(){
    	$idoutlet = $this->input->post("id_outlet");
    	$password = $this->input->post("password");

    	$newdata = array(
                   'id_outlet'  => $idoutlet,
                   'password'     => md5($password),
                   'logged_in' => TRUE
               );
		$this->session->set_userdata($newdata);

		$this->set_result_multiple(array(
            "success"=>true, 
            "desc"=>"Sukses",
            "data" => []
        ));
		$this->print_result();
    }

    public function load_inbox(){
        $type = $this->input->get("type");
        $limit = $this->input->get("limit");
        $offset = $this->input->get("offset");
    	$msg = array(
            "type" => strtoupper($type),
            "offset" => $offset,
            "limit" => $limit
        );
        $message = new CNFMessage();
        $message->set_data($msg);
        //var_dump($message->get_all_data());die();
        

        $SpiSender_payment = new CNFApi(SCApiConstant::API_URL);
        $SpiSender_payment->doCurlPost(SCApiConstant::PATH_LIST_INBOX, $message->get_all_data(), SCApiContentType::JSON);

        $response = "";
        $next = false;
        if (!$SpiSender_payment->isERROR()) {
            $response = $SpiSender_payment->getData();
            if(empty($response)){
                $this->set_result_multiple(array(
                    "success"=>false, 
                    "desc"=>"Data tidak ditemukan",
                    "data" => $response
                ));
            }else{
                $this->set_result_multiple(array(
                    "success"=>true, 
                    "desc"=>$SpiSender_payment->getRD(),
                    "data" => $response
                ));
            }
	        
             
        }else{
        	$this->set_result_multiple(array(
				"success"=>false, 
				"desc"=>$SpiSender_payment->getRD(),
				"data" => $response
			));
        }
		$this->print_result();

    }

    public function unread_message(){
    	$msg = array(
            "type" => "ALL"
        );
        $message = new CNFMessage();
        $message->set_data($msg);
        //var_dump($message->get_all_data());die();
        

        $SpiSender_payment = new CNFApi(SCApiConstant::API_URL);
        $SpiSender_payment->doCurlPost(SCApiConstant::PATH_UNREAD_INBOX, $message->get_all_data(), SCApiContentType::JSON);

        $response = "";
        $next = false;
        if (!$SpiSender_payment->isERROR()) {
            $response = $SpiSender_payment->getData();
            $next = true;
	        $this->set_result_multiple(array(
				"success"=>true, 
				"desc"=>$SpiSender_payment->getRD(),
				"data" => $response
			));
             
        }else{
        	$this->set_result_multiple(array(
				"success"=>false, 
				"desc"=>$SpiSender_payment->getRD(),
				"data" => $response
			));
        }
		$this->print_result();

    }

    public function read_message(){
        $inboxid = $this->input->post("idinbox");
        $msg = array(
            "inboxid" => $inboxid
        );
        $message = new CNFMessage();
        $message->set_data($msg);
        //var_dump($message->get_all_data());die();

        $SpiSender_payment = new CNFApi(SCApiConstant::API_URL);
        $SpiSender_payment->doCurlPost(SCApiConstant::PATH_READ_INBOX, $message->get_all_data(), SCApiContentType::JSON);

        $response = "";
        $next = false;
        if (!$SpiSender_payment->isERROR()) {
            $response = $SpiSender_payment->getData();
            $next = true;
            $this->set_result_multiple(array(
                "success"=>true, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
             
        }else{
            $this->set_result_multiple(array(
                "success"=>false, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
        }
        $this->print_result();

    }

    public function detail_message(){
        $slug = $this->input->get("slug");
        $msg = array(
            "slug" => $slug
        );
        $message = new CNFMessage();
        $message->set_data($msg);
        //var_dump($message->get_all_data());die();

        $SpiSender_payment = new CNFApi(SCApiConstant::API_URL);
        $SpiSender_payment->doCurlPost(SCApiConstant::PATH_DETAIL_INBOX, $message->get_all_data(), SCApiContentType::JSON);

        $response = "";
        $next = false;
        if (!$SpiSender_payment->isERROR()) {
            $response = $SpiSender_payment->getData();
            $next = true;
            $this->set_result_multiple(array(
                "success"=>true, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
             
        }else{
            $this->set_result_multiple(array(
                "success"=>false, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
        }
        $this->print_result();

    }

    public function delete_message(){
        $inboxid = $this->input->post("idinbox");
        $msg = array(
            "inboxid" => $inboxid
        );
        $message = new CNFMessage();
        $message->set_data($msg);
        //var_dump($message->get_all_data());die();

        $SpiSender_payment = new CNFApi(SCApiConstant::API_URL);
        $SpiSender_payment->doCurlPost(SCApiConstant::PATH_DELETE_INBOX, $message->get_all_data(), SCApiContentType::JSON);

        $response = "";
        $next = false;
        if (!$SpiSender_payment->isERROR()) {
            $response = $SpiSender_payment->getData();
            $next = true;
            $this->set_result_multiple(array(
                "success"=>true, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
             
        }else{
            $this->set_result_multiple(array(
                "success"=>false, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
        }
        $this->print_result();

    }

    public function multidelete_message(){
        $inboxid = $this->input->post("idinbox");
        $msg = array(
            "inboxid" => $inboxid
        );
        $message = new CNFMessage();
        $message->set_data($msg);

        $SpiSender_payment = new CNFApi(SCApiConstant::API_URL);
        $SpiSender_payment->doCurlPost(SCApiConstant::PATH_MULTIDELETE_INBOX, $message->get_all_data(), SCApiContentType::JSON);

        $response = "";
        $next = false;
        if (!$SpiSender_payment->isERROR()) {
            $response = $SpiSender_payment->getData();
            $next = true;
            $this->set_result_multiple(array(
                "success"=>true, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
             
        }else{
            $this->set_result_multiple(array(
                "success"=>false, 
                "desc"=>$SpiSender_payment->getRD(),
                "data" => $response
            ));
        }
        $this->print_result();

    }
}
