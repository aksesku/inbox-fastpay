<?php

/**
 * Description of SCMessage
 *
 * @author dian
 */
class CNFMessage {

    const KEY_CRED = "credential_data";
    const KEY_ADD = "additional_data";
    const KEY_USERID = "user_id";
    const KEY_TOKEN = "token";
    const KEY_MSSGID = "messageid";
    const KEY_VIA = "via";
    const KEY_KODE_PRODUK = "kodeProduk";
    const KEY_PROCESS = "process";

    protected $inner_data = array(
        self::KEY_USERID => "",
        "data" => array(),
        self::KEY_CRED => "",
        self::KEY_ADD => ""
    );

    function __construct($class_name = "") {
        $f3 =& get_instance();     
        $f3->load->library('session');
        $f3->load->library('CNFApi');
        $this->set(self::KEY_USERID, $f3->session->userdata('id_outlet'));

        $credential_data = array(
            "id_outlet" => $f3->session->userdata('id_outlet'),
            "password" => $f3->session->userdata('password'),
            "api_key" => SCApiConstant::APIKEY
        );
        $this->set(self::KEY_CRED, $credential_data);

        $additional_data = array(
            "transmission_datetime" => date("Y-m-d H:i:s"),
            "uuid" => "",
            "tokenizer" => "",
            "app_id" => SCApiConstant::APPID,
            "device_information" => $_SERVER['HTTP_USER_AGENT']
        );
        $this->set(self::KEY_ADD, $additional_data);
        
        
    }

    function exists($key) {
        return array_key_exists($key, $this->inner_data);
    }

    function set($key, $val) {
        $this->inner_data[$key] = $val;
    }

    function &get($key) {
        return $this->inner_data[$key];
    }

    function clear($key) {
        unset($this->inner_data[$key]);
    }

    function set_data($value) {
        $this->inner_data["data"]= $value;
    }

    

    function get_data() {
        return $this->inner_data["data"];
    }
    
    function get_all_data(){
        return $this->inner_data;
    }
    
    function __toString() {
        return json_encode($this->inner_data);
    }
    
    function parse_to_model($json) {
        return $this->inner_data=json_decode($json);
    }

}
