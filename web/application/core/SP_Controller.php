<?php

class SP_Controller extends CI_Controller {
    
    private $table;
    private $params;
    private $result;

    function __construct()
    {
        parent::__construct();
        
        $this->load->library('session');
        
        $logged_in = $this->session->userdata('id_outlet'); 

        if(!$logged_in && !preg_match("#/login#", $this->input->server("PHP_SELF")))
        {
            header("location: login");
        }
    }
    
    protected function view($filename, $data = array())
    {
        $theme = $this->load->view("template", $data, true);
        
        $page = $this->load->view($filename, $data, true);
        
        exit(str_replace("{CONTENT}", $page, $theme));
    }
    
    public function set_table($param) {
        $this->table = $param;
    }
    
    public function get_table() {
        return $this->table;
    }
    
    public function set_param($key, $value) {
        $this->params[$key] = $value;
    }
    
    public function get_param($key, $unset = FALSE) {
        if (array_key_exists($key, $this->params))
            if ($unset) {
                $data = $this->params[$key];
                unset($this->params[$key]);
                return $data;
            } else
                return $this->params[$key];
        else 
            return false;
    }
    
    public function get_all_param() {
        return $this->params;
    }
    
    public function clear_param(){
        unset($this->params);
        $this->params = array();
    }
    
    public function set_param_multiple($data, $pref="") {
        if (is_array($data))
            foreach ($data as $key => $value)
                $this->set_param($pref.$key, $value);
    }
    
    public function get($print_result = TRUE) {
//        $param = $this->input->get() ? $this->input->get() : $this->input->post();
        if ($this->input->get()) $this->set_param_multiple($this->input->get());
        
        $limit = $this->get_param('limit') ? $this->get_param('limit', TRUE) : 18446744073709551615;
        $offset = ($this->get_param('offset') || $this->get_param('offset') == 0) ? $this->get_param('offset', TRUE) : 0;
        $order = $this->get_param('order') ? $this->get_param('order', TRUE) : false;
        
        $this->db->where($this->get_all_param());
//        $this->db->limit($limit, $offset);
        if ($order) $this->db->order_by ($order);
        $data = $this->db->get($this->table);
        
        $this->db->flush_cache();
        $this->db->where($this->get_all_param());
        $count = $this->db->count_all_results($this->table);
        
        if ($this->db->_error_message() || $this->db->_error_number())
            $this->set_result_multiple(array("success"=>false, "desc"=>"DB - " . $this->db->_error_message()));
        elseif ($data->num_rows() > 0)
//            $this->set_result ("data", $data->result());
            $this->set_result_multiple (array("data"=>$data->result(), "count"=>$count));
        else 
            $this->set_result_multiple(array("success"=>false, "desc"=>"no data found"));
        
        if ($print_result) 
            $this->print_result();
        else 
            return $this->result;
    }
    
    public function add() {
        if ($this->input->post()) {
            $this->set_param_multiple($this->input->post());
            $this->db->insert($this->table, $this->get_all_param());

            if ($this->db->_error_message() || $this->db->_error_number())
                $this->set_result_multiple(array("success"=>false, "desc"=>"DB - failed to insert. " . $this->db->_error_message()));
            else
                $this->set_result ("data", array("insert_id"=>$this->db->insert_id()));
        } else {
            $this->set_result_multiple(array("success"=>false, "desc"=>"no data submitted"));
        }
        $this->print_result();
    }
    
    public function map() {
        $this->set_result("data", $this->db->field_data($this->table));
        $this->print_result();
    }
    
    public function set_result($key, $value) {
        $this->result[$key] = $value;
    }
    
    public function unset_result($key) {
        if (array_key_exists($key, $this->result))
            unset ($this->result[$key]);
    }
    
    public function set_result_multiple($data) {
        if (is_array($data))
            foreach ($data as $key => $value)
                $this->set_result ($key, $value);
    }
    
    public function print_result($exit = false) {
        echo json_encode($this->result);
        if ($exit) exit();
    }
    
    public function encrypt($unecriptedtext) {        
        $rsapublickeypath = APPPATH."libraries/publickey.pem";
        $this->load->library('CastleCrypt');
        $encryptorclass = new CastleCrypt();
        $encryptorclass->setPublicKey(file_get_contents($rsapublickeypath));
        $res = urlencode(base64_encode($encryptorclass->encrypt($unecriptedtext)));

        return $res;
//        echo $res;
    }
    
    public function decrypt($encriptedtext) {
//    public function decrypt() {
        $rsaprivatekeypath = APPPATH."libraries/privatekey.pem";
        $this->load->library('CastleCrypt');
        $decryptorclass = new CastleCrypt();
        $decryptorclass->setPrivateKey(file_get_contents($rsaprivatekeypath));
        $res = $decryptorclass->decrypt(base64_decode(urldecode($encriptedtext)));
//        $res = $decryptorclass->decrypt(base64_decode(urldecode($this->input->post('code'))));

        return $res;
//        echo $res;
    }
    public function format_uang($number, $curr = "Rp") {

        $number = intval($this->_replace_separation($number));

        $jumlah_desimal = "0";
        $pemisah_desimal = ",";
        $pemisah_ribuan = ".";

        $result = $curr . " " . number_format($number, $jumlah_desimal, $pemisah_desimal, $pemisah_ribuan);
        return($result);
    }
    function _replace_separation($value) {
        $value = str_replace(",", "", $value);
        $value = str_replace(".", "", $value);
        $value = str_replace("-", "", $value);
        return $value;
    }
    public function insert_email($to, $subject, $content, $cc) {
//        if ($to && $subject && $content)
            $this->db->insert('sp_email', array('to' => $to, 'subject' => $subject, 'content' => $content, 'cc' => $cc));
            
        return true;
    }
    function bulan($param) {
        $BulanIndo = array(
            "Januari", "Februari", "Maret", "April",
            "Mei", "Juni", "Juli", "Agustus",
            "September", "Oktober", "November", "Desember"
        );
        return $BulanIndo[intval($param)-1];
    }
    
}
